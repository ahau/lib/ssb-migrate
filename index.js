const { promisify: p } = require('util')
const fs = require('fs')
const obz = require('obz')
const { join } = require('path')
const pull = require('pull-stream')

const migrationDB = require('./migration-db')
const isValidMigrationName = require('./is-valid-migration-name')
const Logger = require('./logger')

module.exports = class Migrate {
  constructor (ssb, path, opts = {}) {
    // opts = {
    //   verbose: Boolean,                    (default: false),
    //   migrations: { fileName: migration },
    //   resetIfCorrupt: Boolean              (default: false)
    // }

    this.ssb = ssb
    this.path = path
    this.logger = opts.verbose && Logger(ssb)

    this.db = migrationDB(ssb.config.path, { resetIfCorrupt: opts.resetIfCorrupt })

    /* load persisted state */
    this.isReady = obz()
    this.migrationState = {
      // complete: []
      // todo: []
    }
    this.migrations = opts.migrations // { fileName: function }
    this.error = null

    /* load the migrationState persisted to disk */
    this.migrationState = this.db.load()

    const complete = new Set(this.migrationState.complete)
    let todo

    /* load the migration names in path */
    const fileNames = fs.readdirSync(path)

    if (this.migrations) {
      const missing = fileNames.filter(fileName => !this.migrations[fileName])
      if (missing.length) {
        throw new Error(`missing migrations: ${missing.join(', ')}`)
      }

      todo = Object.keys(this.migrations)
        .filter(fileName => !complete.has(fileName))
        .sort()

      // later - if opts.migrations is provided, still check we have all
      // the migrations from that folder included
      //
    } // eslint-disable-line
    else {
      todo = fileNames
        .filter(fileName => !complete.has(fileName))

      // build this.migrations
      this.migrations = todo.reduce((acc, fileName) => {
        acc[fileName] = require(join(this.path, fileName))
        return acc
      }, {})
    }

    this.error = migrationError(todo)
    this.migrationState.todo = todo
    this.isReady.set(true)
  }

  state (cb) {
    if (cb === undefined) return p(this.state).call(this)

    this.isReady.once(() => {
      if (this.error) return cb(this.error)
      cb(null, this.migrationState)
    })
  }

  run (cb) {
    if (cb === undefined) return p(this.run).call(this)

    const done = () => {
      if (this.db.isSaving) return setTimeout(done, 100)

      if (this.logger) console.timeEnd('\nmigrations complete')
      cb(null)
    }

    this.isReady.once(() => {
      if (this.error) return cb(this.error)
      if (this.migrationState.todo.length === 0) return cb(null)

      if (this.logger) {
        console.log(`\nmigrations to run: ${this.migrationState.todo.length}`)
        console.time('\nmigrations complete')
      }

      pull(
        pull.values(this.migrationState.todo),
        pull.asyncMap((fileName, cb) => {
          const migration = this.migrations[fileName]

          this.logger && this.logger.start(fileName)

          migration.up(this.ssb, {}, (err) => {
            if (err) {
              cb(new Error(`migration ${fileName} failed: ${err.message}`))
              return
            }

            this.logger && this.logger.stop(fileName)
            cb(null, fileName)
          })
        }),
        pull.drain(
          (fileName) => {
            this.migrationState.complete.push(fileName)
            this.migrationState.todo = this.migrationState.todo.filter(name => name !== fileName)

            this.db.save(this.migrationState, (err) => {
              if (err) throw err
            })
          },
          (err) => {
            if (err) return cb(err)
            done()
          }
        )
      )
    })
  }
}

function migrationError (fileNames) {
  const badMigrations = fileNames.filter(fileName => !isValidMigrationName(fileName))

  if (badMigrations.length) return new Error(`invalid migration fileNames: ${fileNames.join(',')}`)
  else return null
}
