const fs = require('fs')
const { join } = require('path')

module.exports = function migrationDB (ssbPath, opts = {}) {
  const dbPath = join(ssbPath, 'migration.json')

  let savingCount = 0

  return {
    load () {
      let content
      try {
        content = fs.readFileSync(dbPath, 'utf8')
      } catch (err) {
        if (err.message.match(/no such file or directory/)) {
          // if migration.json can't be loaded it's because it doesn't exit yet
          return { todo: [], complete: [] }
        }

        throw err // a legitamite error
      }

      try {
        content = JSON.parse(content)
      } catch (err) {
        // if migration.json can't be loaded because the JSON is corrupt
        console.error('trouble parsing JSON', err)

        if (opts.resetIfCorrupt) return { todo: [], complete: [] }
        else throw err
      }

      // TODO validate the content!
      return content
    },
    save (state, cb) {
      savingCount++

      fs.writeFile(dbPath, JSON.stringify(state, null, 2), (err) => {
        savingCount--
        cb(err)
      })
    },
    get isSaving () {
      return savingCount > 0
    }
  }
}
