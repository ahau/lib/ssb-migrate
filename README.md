# ssb-migrate

Helper for performing database migrations.
Records database migrations that have been done and performs those which haven't been done yet.

## Example Usage

```js
const Migrate = require('ssb-migrate')

const migrate = new Migrate(ssb, path.join(__dirname, '../migrations'))

migrate.run((err) => {
  // ..
})
```

## Migration files

mirgation files must:
- be named in format `${UnixTime}-${kebab-case-description}.js`
- export and Object with function `up`

```js
// 1632689651072-add-kaitiki-subgroups.js

modules.exports = {
  up (ssb, misc, cb) {
    // does the migration
    cb(null) // make sure you remember to callback
  },

  // down (ssb, misc, cb) {
  //   // undoes the migration
  // }
}
```

## API

### `new Migrate(ssb, path, opts) => migrate`

takes:
- `ssb` - a scuttlebutt instance
- `path` - the absolute path to a folder with migrations in it
- `opts` (optional) object
    - `opts.verbose` *Boolean* - if set to true logs migration progress
    - `opts.migration` *Object*
        - manually provide the migrations, useful for bundling
        - format: `{ fileName: migration }`
    - `opts.resetIfCorrupt` *Boolean*
        - this module uses a JSON file to record the migrations it has completed
        - if this is corrupted, the default action is to throw. this lets you over-ride and tell `ssb-migrate` to just start over with everything
        - default: `false`
loads:
- list of migrations in the `path` folder
- the "database" (file) containing list of completed migrations
    - this file is stored in `ssb.config.path`

returns `migrate` instance


### `migrate.state(cb)`

Calls back with the current state of migrations, in format:

```js
{
  complete: []
  todo: [
    '1632689651072-add-kaitiki-subgroups'
  ]
}
```

_alias await migrate.state()_

### `migrate.run(cb)`

Runs outstanding migrations

## TODO

- add validations
  - protect against migrations being inserted between migrations that have already been run
  - force migrations to start with a timestamp for order
