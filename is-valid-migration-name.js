const regexp = /^\d{13,}\d*-\w+.*\.js$/

module.exports = function isValidMigrationName (name) {
  return regexp.test(name)
}
