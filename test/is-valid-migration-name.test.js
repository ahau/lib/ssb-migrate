const test = require('tape')
const isValid = require('../is-valid-migration-name')

test('is-valid-migration-name', t => {
  t.true(isValid(`${Date.now()}-yes.js`))
  t.true(isValid(`1${Date.now()}-yes.js`))

  t.false(isValid(`${Date.now()}.js`))
  t.false(isValid(`${Date.now()}-no.json`))
  t.false(isValid('some-file.js'))

  t.end()
})
