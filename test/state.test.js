const Server = require('scuttle-testbot')
const test = require('tape')
const fs = require('fs/promises')
const path = require('path')
const os = require('os')

const Migrate = require('../')

test('#state', async t => {
  const ssb = Server()

  /* setup migration folder */
  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  /* place one migration in our folder */
  const migOne = `${Date.now()}-one.js`
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        cb(null)
      }
    }
    `
  )

  let migrate = new Migrate(ssb, migrationsPath)

  let state = await migrate.state()

  const initialState = {
    complete: [],
    todo: [
      migOne
    ]
  }

  t.deepEqual(state, initialState, 'initial state')

  await migrate.run()

  state = await migrate.state()
  const doneState = {
    complete: [
      migOne
    ],
    todo: []
  }

  t.deepEqual(state, doneState, 'change to done state')

  /* check persistence */
  migrate = new Migrate(ssb, migrationsPath)
  state = await migrate.state()
  t.deepEqual(state, doneState, 'persisted state!')

  ssb.close()
  t.end()
})

test('#state (previous completed state is preserved)', async t => {
  const ssb = Server()

  /* record previous state */
  const migZero = `${Date.now()}-zero.js`
  const previousState = {
    complete: [migZero],
    todo: []
  }
  await fs.writeFile(path.join(ssb.config.path, 'migration.json'), JSON.stringify(previousState))

  /* setup migration folder */
  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  /* place one migration in our folder */
  const migOne = `${Date.now() + 100}-one.js`
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        cb(null)
      }
    }
    `
  )

  let migrate = new Migrate(ssb, migrationsPath)

  let state = await migrate.state()

  const initialState = {
    complete: [
      migZero
    ],
    todo: [
      migOne
    ]
  }

  t.deepEqual(state, initialState, 'initial state')

  await migrate.run()

  state = await migrate.state()
  const doneState = {
    complete: [
      migZero,
      migOne
    ],
    todo: []
  }

  t.deepEqual(state, doneState, 'change to done state')

  /* check persistence */
  migrate = new Migrate(ssb, migrationsPath)
  state = await migrate.state()
  t.deepEqual(state, doneState, 'persisted state!')

  ssb.close()
  t.end()
})

test('#state (bad migration)', async t => {
  const ssb = Server()

  /* setup migration folder */
  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  /* place one migration in our folder */
  const migOne = 'bad-migration-name.js'
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        cb(null)
      }
    }
    `
  )

  const migrate = new Migrate(ssb, migrationsPath)

  const result = await migrate.state()
    .catch(err => ({ error: err }))

  t.match(result.error.message, /invalid migration fileNames: bad-migration-name.js/)

  ssb.close()
  t.end()
})
