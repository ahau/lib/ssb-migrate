const Server = require('scuttle-testbot')
const test = require('tape')
const fs = require('fs/promises')
const path = require('path')
const os = require('os')

const Migrate = require('../')

test('#run', async t => {
  t.plan(3)
  const ssb = Server()

  let count = 0
  ssb.post(m => {
    if (m.value.content.type !== 'migration-test') throw new Error(`unknown ${m.value.content.type} msg`)

    count++
    if (count === 1) t.pass('migration is run')
  })

  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  const migOne = `${Date.now()}-one.js`
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        ssb.publish({ type: 'migration-test' }, cb)
      }
    }
    `
  )

  let migrate = new Migrate(ssb, migrationsPath)
  await migrate.run()

  // check running again does not result in double migration!
  await migrate.run()
  t.equal(count, 1, 'calling #run does not re-run migrations')

  migrate = new Migrate(ssb, migrationsPath)
  await migrate.run()
  t.equal(count, 1, 'calling #run does not re-run migrations (on fresh start)')

  ssb.close()
})

test('#run (multiple migrations)', async t => {
  t.plan(4)
  const ssb = Server()

  let count = 0
  ssb.post(m => {
    const { type, expectedCount } = m.value.content

    if (type === 'migration-test') {
      const countHere = ++count
      t.equal(expectedCount, countHere, `migration ${countHere}`)
    }
  })

  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  const migOne = `${Date.now()}-one.js`
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        ssb.publish({ type: 'migration-test', expectedCount: 1 }, cb)
      }
    }
    `
  )
  const migTwo = `${Date.now() + 100}-two.js`
  await fs.writeFile(
    path.join(migrationsPath, migTwo),
    `
    const MAX_DELAY = 0 // change this to see progress output
    const PUBLISH = 10

    module.exports = {
      up (ssb, misc, cb) {
        let count = 0

        next()
        function next (err) {
          if (err) throw err

          count++
          if (count > PUBLISH) ssb.publish({ type: 'migration-test', expectedCount: 2 }, cb)
          else ssb.publish({ type: 'count', count }, (err) => {
            const r = Math.random()
            setTimeout(() => next(err), r * r * MAX_DELAY)
          })
        }
      }
    }
    `
  )

  let migrate = new Migrate(ssb, migrationsPath, { verbose: false })
  await migrate.run()

  const expectedState = {
    complete: [migOne, migTwo],
    todo: []
  }

  let state = await migrate.state()
  t.deepEqual(state, expectedState, 'completes two migrations fine')

  migrate = new Migrate(ssb, migrationsPath)
  state = await migrate.state()
  t.deepEqual(state, expectedState, 'persisted')

  ssb.close()
})

test('#run (bad migration name)', async t => {
  const ssb = Server()

  /* setup migration folder */
  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  /* place one migration in our folder */
  const migOne = 'bad-migration-name.js'
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        cb(null)
      }
    }
    `
  )

  const migrate = new Migrate(ssb, migrationsPath)

  const result = await migrate.run()
    .catch(err => ({ error: err }))

  t.match(result.error.message, /invalid migration fileNames: bad-migration-name.js/)

  ssb.close()
  t.end()
})

test('#run (error in migration)', async t => {
  const ssb = Server()

  /* setup migration folder */
  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  /* place one migration in our folder */
  const migOne = `${Date.now()}-one.js`
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        cb(new Error('oh no, an error'))
      }
    }
    `
  )

  const migrate = new Migrate(ssb, migrationsPath)

  const result = await migrate.run()
    .catch(err => ({ error: err }))

  t.match(result.error.message, new RegExp(`migration ${migOne} failed:.*oh no`))

  ssb.close()
  t.end()
})

/* opts.migrations variants */

test('#run (opts.migrations)', async t => {
  t.plan(3)
  const ssb = Server()

  let count = 0
  ssb.post(m => {
    if (m.value.content.type !== 'migration-test') throw new Error(`unknown ${m.value.content.type} msg`)

    count++
    if (count === 1) t.pass('migration is run')
  })

  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  const migOne = `${Date.now()}-one.js`
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        cb(new Error('THIS FILE SHOULD NOT BE RUN'))
      }
    }
    `
  )
  const migOneFunction = {
    up (ssb, misc, cb) {
      ssb.publish({ type: 'migration-test' }, cb)
    }
  }

  let migrate = new Migrate(ssb, migrationsPath, {
    migrations: { [migOne]: migOneFunction }
  })
  await migrate.run()

  // check running again does not result in double migration!
  await migrate.run()
  t.equal(count, 1, 'calling #run does not re-run migrations')

  migrate = new Migrate(ssb, migrationsPath, {
    migrations: { [migOne]: migOneFunction }
  })
  await migrate.run()
    .catch(err => t.fail(err))
  t.equal(count, 1, 'calling #run does not re-run migrations (on fresh start)')

  ssb.close()
})

test('#run (opts.migrations - checks migrations provided for everything in dir)', async t => {
  t.plan(4)
  const ssb = Server()

  let count = 0
  ssb.post(m => {
    if (m.value.content.type !== 'migration-test') throw new Error(`unknown ${m.value.content.type} msg`)
    count++
    if (count === 2) t.pass('migrations complete')
    if (count > 2) t.fail('should not post this many messages')
  })

  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  const migOne = `${Date.now()}-one.js`
  const migTwo = `${Date.now()}-two.js`
  await fs.writeFile(path.join(migrationsPath, migOne), '---')
  await fs.writeFile(path.join(migrationsPath, migTwo), '---')

  let order = 0
  const migrations = {
    [migTwo]: {
      up (ssb, misc, cb) {
        t.equal(++order, 2, 'run second')
        ssb.publish({ type: 'migration-test' }, cb)
      }
    },
    [migOne]: {
      up (ssb, misc, cb) {
        t.equal(++order, 1, 'run first')
        ssb.publish({ type: 'migration-test' }, cb)
      }
    }
  }

  const migrate = new Migrate(ssb, migrationsPath, { migrations })
  await migrate.run()

  /* make a migration file, forget to provide in opts.migration */
  const migThree = `${Date.now()}-three.js`
  await fs.writeFile(path.join(migrationsPath, migThree), '---')

  t.throws(
    () => new Migrate(ssb, migrationsPath, { migrations }),
    /missing migrations.*three.js/,
    'throw if opts.migrations is missing any..'
  )

  ssb.close()
})
