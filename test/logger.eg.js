const Server = require('scuttle-testbot')
const fs = require('fs/promises')
const path = require('path')
const os = require('os')

const Migrate = require('../')

async function loggerExample () {
  const ssb = Server()

  const migrationsPath = path.join(os.tmpdir(), `migrations-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(migrationsPath)

  const migOne = `${Date.now()}-one.js`
  await fs.writeFile(
    path.join(migrationsPath, migOne),
    `
    module.exports = {
      up (ssb, misc, cb) {
        ssb.publish({ type: 'migration-test', expectedCount: 1 }, cb)
      }
    }
    `
  )
  const migTwo = `${Date.now() + 100}-two.js`
  await fs.writeFile(
    path.join(migrationsPath, migTwo),
    `
    const MAX_DELAY = 100 // change this to see progress output
    const PUBLISH = 20

    module.exports = {
      up (ssb, misc, cb) {
        let count = 0

        next()
        function next (err) {
          if (err) throw err

          count++
          if (count > PUBLISH) ssb.publish({ type: 'migration-test', expectedCount: 2 }, cb)
          else ssb.publish({ type: 'count', count }, (err) => {
            const r = Math.random()
            setTimeout(() => next(err), r * r * MAX_DELAY)
          })
        }
      }
    }
    `
  )

  const migrate = new Migrate(ssb, migrationsPath, { verbose: true })
  await migrate.run()

  ssb.close()
}

loggerExample()
