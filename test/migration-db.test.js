const test = require('tape')
const fs = require('fs/promises')
const path = require('path')
const os = require('os')
const { promisify: p } = require('util')

const MigrationDB = require('../migration-db')

test('migration-db', async t => {
  const ssbPath = path.join(os.tmpdir(), `migrations-db-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(ssbPath)

  const db = MigrationDB(ssbPath)
  const content = db.load()

  t.deepEqual(content, { todo: [], complete: [] })
  content.todo = ['dog']
  content.complete = ['cat']
  await p(db.save)(content)

  t.deepEqual(db.load(), { todo: ['dog'], complete: ['cat'] })

  t.end()
})

test('migration-db (corrupt db)', async t => {
  const ssbPath = path.join(os.tmpdir(), `migrations-db-${Date.now()}-${Math.random() * 1000 | 0}`)
  await fs.mkdir(ssbPath)

  await fs.writeFile(path.join(ssbPath, 'migration.json'), 'some junk')

  let db = MigrationDB(ssbPath)

  t.throws(() => db.load(), /JSON/)

  db = MigrationDB(ssbPath, { resetIfCorrupt: true })
  t.deepEqual(db.load(), { todo: [], complete: [] })

  t.end()
})
