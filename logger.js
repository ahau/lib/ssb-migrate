const PROGRESS = '.'
const EMPTY = ' '
const WIDTH = 60
const TICK = '\x1b[32m✓\x1b[0m' // green

module.exports = function (ssb) {
  let migrationName
  let listener
  let count = 0
  const progress = new Array(WIDTH).fill(EMPTY)

  function updateProgress (count) {
    if (count === 0) return progress.fill(EMPTY)

    progress.fill(PROGRESS, 0, count % WIDTH)
    progress.fill(EMPTY, WIDTH - count % WIDTH)
  }

  return {
    start (name) {
      if (listener && name !== migrationName) {
        throw new Error(`cannot start ${name} while ${migrationName} is running`)
      }

      migrationName = name

      listener = ssb.post(m => {
        count++
        updateProgress(count)

        process.stdout.write(`\r    ${migrationName} ${progress.join('')}`)
      })
    },
    stop () {
      listener && listener()
      listener = null

      updateProgress(0)

      process.stdout.write(`\r  ${TICK} ${migrationName} ${progress.join('')}\n`)

      migrationName = null
      count = 0
    }
  }
}
